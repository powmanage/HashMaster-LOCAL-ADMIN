echo "请务必确保H-降低线程.sh 及 I-恢复线程至正常值.sh设置正确"
if [ "$(dirname "$(pwd)")" = '/root' ]; then
    echo "上一层目录是 root, 允许执行"
else
    echo "上一层目录不是root，为避免程序运行在非矿机上，必须要求root用户，请修改"
    exit
fi

sleep 2
current_path=$(cd $(dirname $0);pwd)
echo "current_path=$current_path"
cd $current_path

originIpListFile="./groupSshList/ip_list_00001.txt"
if [ ! -f "$originIpListFile" ];then
    echo "FAILED"
    echo -e "\e[1;33;41m 当前目录不存在 ip_list.txt 文件，请编辑A-一键生成ip_list.txt文件用于批量控制.sh 参数并执行以生成文>件.  \e[0m"
    exit
fi

isContinue='y'
echo -e "\e[1;33;41m 第一步：测试前三台机器重启进程功能，如选择n则表示不测试，直接进入循环定时重启（可选）  \e[0m"
echo "输入y继续. 首次操作，将仅操作前三台机器"
read -p '继续发送<测试>操作?[y/n]' isContinue
cmd='bash /root/HashMaster-LOCAL-ADMIN/HM-LINUX/Kernel/qcoin-linux/newest/runqubic.sh'
echo "请注意：当前执行重启的指令为: $cmd，请务必确认指令正确"
if [[ "$isContinue" == y ]];then
   parallel-ssh -h ip_list_test.txt -P 'screen -r Coin-Running -X quit'
   parallel-ssh -h ip_list_test.txt -P $cmd
   
   echo -e "\e[1;33;41m 第二步：测试已完成，是否立刻发送给所有机器 ？  \e[0m"
   read -p '继续发送<全部机器>操作?[y/n]' isContinue
   if [[ "$isContinue" == y ]];then
       directory="./groupSshList"
       ls "$directory"/*.txt | sort | while read -r file; do
             # 输出文件名（不包括路径）
             filename=$(basename "$file")
             ipFile=$directory/$filename
             echo "当前执行$ipFile下机器，如需要可另开窗口以确认当前机器IP列表"
             parallel-ssh -h $ipFile -P 'screen -r Coin-Running -X quit'
             parallel-ssh -h $ipFile -P $cmd
	     sleep 8
       done
       echo "所有机器已执行完成"
   fi
fi
directory="./groupSshList"
echo "发送已完成（或已取消），将循环执行脚本\n\n\n"
echo -e "\e[1;33;41m 第三步：循环定时重启$cmd关联进程  \e[0m"
echo "输入y继续. 将操作所有机器"
read -p '继续<所有机器循环执行>操作?[y/n]' isContinue
if [[ "$isContinue" == y ]];then
    echo “脚本将循环执行，将在10:00及22:00执行所有客户机 重启$cmd关联进程”
    now=$(date +%H:%M)

    while true; do
      now=$(date +%H:%M)
      if [ "$now" == "10:00" ]; then
	echo "现在时间为 10:00, 执行重启$cmd关联进程.sh"
	ls "$directory"/*.txt | sort | while read -r file; do
             # 输出文件名（不包括路径）
             filename=$(basename "$file")
             ipFile=$directory/$filename
             echo "当前执行$ipFile下机器，如需要可另开窗口以确认当前机器IP列表"
             parallel-ssh -h $ipFile -P 'screen -r Coin-Running -X quit'
             parallel-ssh -h $ipFile -P $cmd
             sleep 8
        done
	sleep 80
	echo "执行 重启$cmd关联进程 完成，待20点将再次执行"
      fi
      
      if [ "$now" == "IGNORE" ]; then
        echo "现在时间为 22:00, 执行重启$cmd关联进程.sh"
	ls "$directory"/*.txt | sort | while read -r file; do
             # 输出文件名（不包括路径）
             filename=$(basename "$file")
             ipFile=$directory/$filename
             echo "当前执行$ipFile下机器，如需要可另开窗口以确认当前机器IP列表"
             parallel-ssh -h $ipFile -P 'screen -r Coin-Running -X quit'
             parallel-ssh -h $ipFile -P $cmd
             sleep 8
        done
	sleep 80
	echo "执行 重启$cmd关联进程 完成，待10点将再次执行"
      fi
      echo "现在时间为：" $now
      sleep 10
    done
fi
echo “取消循环执行”
