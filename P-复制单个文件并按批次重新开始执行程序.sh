#!/bin/bash
if [ "$(dirname "$(pwd)")" = '/root' ]; then
    echo "上一层目录是 root, 允许执行"
else
    echo "上一层目录不是root，为避免程序运行在非矿机上，必须要求root用户，请修改"
    exit
fi

echo -e "\e[1;33;41m 使用此脚本前，请确保你已正确修改了当前目录下的HM-LINUX的config.ini文件及CoinsConfig下对应币种的ini文件! 通常。需要修改币种/管理员地址/管理员密码/钱包地址等内容.  \e[0m"
sleep 2
echo "call 1-同步本机软件及配置至所有客户机.sh"
current_path=$(cd $(dirname $0);pwd)
echo "current_path=$current_path"
cd $current_path

originIpListFile="./groupSshList/ip_list_00001.txt"
if [ ! -f "$originIpListFile" ];then
    echo "FAILED"
    echo -e "\e[1;33;41m 当前目录不存在 ./groupSshList/1.txt 文件，请编辑A-一键生成ip_list_A.txt文件用于批量控制.sh 参数并执行以生成文件.  \e[0m"
    exit
fi

# 检查是否提供了参数
if [ "$#" -ne 1 ]; then
    echo "请输入待复制的文件绝对路径 Usage: $0 <parameter>"
    exit 1
fi

echo "待复制的文件绝对路径: $1"


isContinue='y'
echo "将依次执行客户机上6-完全退出HashMaster.sh / 将本机传至客户机 / 1-HashMaster-后台执行程序及后台运行本地更新.sh 操作，请确保IP已经正确配置，输入y继续. 首次操作，将仅操作前三台机器"
read -p '继续<测试>操作?[y/n]' isContinue
if [[ "$isContinue" == y ]];then
    parallel-scp -p 8 -h ip_list_test.txt -l root $1 $1
    parallel-ssh -p 32 -h ip_list_test.txt -P 'bash /root/HashMaster-LOCAL-ADMIN/HM-LINUX/6-完全退出HashMaster.sh'
    parallel-ssh -p 32 -h ip_list_test.txt -P 'bash /root/HashMaster-LOCAL-ADMIN/HM-LINUX/1-HashMaster-后台执行程序及后台运行本地更新.sh'

    read -p '继续<发送至所有机器>操作?[y/n]' isContinue
    if [[ "$isContinue" == y ]];then
	 echo "已继续发送至所有机器"
	 # 指定目录路径
         directory="./groupSshList"
         ls "$directory"/*.txt | sort | while read -r file; do
             # 输出文件名（不包括路径）
             filename=$(basename "$file")
             ipFile=$directory/$filename
	     echo "当前执行$ipFile下机器，如需要可另开窗口以确认当前机器IP列表"
             echo "（$ipFile）第一步：执行客户机 6-完全退出HashMaster.sh 脚本"
	     parallel-scp -p 8 -h $ipFile -l root $1 $1
             parallel-ssh -p 32 -h $ipFile -P 'bash /root/HashMaster-LOCAL-ADMIN/HM-LINUX/6-完全退出HashMaster.sh'
             parallel-ssh -p 32 -h $ipFile -P 'bash /root/HashMaster-LOCAL-ADMIN/HM-LINUX/1-HashMaster-后台执行程序及后台运行本地更新.sh'
	     echo "$ipFile机器操作已完成，等待8秒，将继续自动开始发送至下一组机器"
	     sleep 15
         done
         echo "机器操作已完成"

	 rm -rf HM-LINUX.zip
	 exit
    fi
    echo "已取消发送至所有机器"
fi
echo "已取消测试操作"

