#!/bin/bash
if [ "$(dirname "$(pwd)")" = '/root' ]; then
    echo "上一层目录是 root, 允许执行"
else
    echo "上一层目录不是root，为避免程序运行在非矿机上，必须要求root用户，请修改"
    exit
fi

sleep 2
current_path=$(cd $(dirname $0);pwd)
echo "current_path=$current_path"
cd $current_path

originIpListFile="./ip_list.txt"
if [ ! -f "$originIpListFile" ];then
    echo "FAILED"
    echo -e "\e[1;33;41m 当前目录不存在 ip_list.txt 文件，请编辑A-一键生成ip_list.txt文件用于批量控制.sh 参数并执行以生成文件.  \e[0m"
    exit
fi

isContinue='y'
echo "输入y继续. 首次操作，将仅操作前三台机器"
read -p '继续<测试>操作?[y/n]' isContinue
if [[ "$isContinue" == y ]];then
    parallel-scp -p 8 -h ip_list_test.txt -l root G-更新本机时间命令.sh /root/G-更新本机时间命令.sh
    parallel-ssh -h ip_list_test.txt -P 'bash /root/G-更新本机时间命令.sh'
    echo "用于测试的前三台机器已操作完成，请ssh进入到对应客户机检查结果,输入y继续操作所有机器"
    read -p '继续<发送至所有机器>操作?[y/n]' isContinue
    if [[ "$isContinue" == y ]];then
	 echo "已继续发送至所有机器"
	 parallel-scp -p 8 -h ip_list.txt -l root G-更新本机时间命令.sh /root/G-更新本机时间命令.sh
         parallel-ssh -h ip_list.txt -P 'bash /root/G-更新本机时间命令.sh'
	 exit
    fi
    echo "已取消发送至所有机器"
fi
echo "已取消测试操作"
