#!/bin/bash
echo "call F-为本机配置root用户可登录.sh"
current_path=$(cd $(dirname $0);pwd)
echo "current_path=$current_path"
cd $current_path

isContinue='y'
read -p '继续<F-为本机配置root用户可登录.sh>操作?[y/n]' isContinue
if [[ "$isContinue" == y ]];then
    echo "为本机配置root用户可登录"
    sed -i 's/#   StrictHostKeyChecking ask/StrictHostKeyChecking no/g' /etc/ssh/ssh_config
    sed -i 's/#PermitRootLogin prohibit-password/PermitRootLogin yes/g' /etc/ssh/sshd_config
    service ssh restart
fi
echo "已取消测试操作"
echo "免密登录 ssh-keygen -t rsa \n
ssh-copy-id -i /root/.ssh/id_rsa.pub root@172.16.1.2"
