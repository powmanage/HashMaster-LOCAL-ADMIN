#!/bin/bash
if [ "$(dirname "$(pwd)")" = '/root' ]; then
    echo "上一层目录是 root, 允许执行"
else
    echo "上一层目录不是root，为避免程序运行在非矿机上，必须要求root用户，请修改"
    exit
fi

echo -e "\e[1;33;41m 使用此脚本, 将移除所有客户机HM-localip_proxyIp_List.txt文件，所有客户机将从默认的中转IP运行 ！  \e[0m"
sleep 2
echo "call 7-移除所有客户机HM-localip_proxyIp_List.txt文件.sh"
current_path=$(cd $(dirname $0);pwd)
echo "current_path=$current_path"
cd $current_path

isContinue='y'
read -p '继续<测试>操作?[y/n]' isContinue
if [[ "$isContinue" == y ]];then
    echo "（测试）第一步：执行客户机 6-完全退出HashMaster.sh 脚本"
    parallel-ssh -p 8 -h ip_list_test.txt -P 'bash '$current_path'/HM-LINUX/6-完全退出HashMaster.sh'
    echo "（测试）第二步: 将HM-localip_proxyIp_List.txt文件删除"
    parallel-ssh -p 8 -h ip_list_test.txt -P 'bash '$current_path'/E-移除HM-localip_proxyIp_List.txt文件.sh'
    echo "（测试）第三步：执行客户机 1-HashMaster-后台执行程序及后>台运行本地更新.s 脚本"
    parallel-ssh -p 8 -h ip_list_test.txt -P 'bash '$current_path'/HM-LINUX/1-HashMaster-后台执行程序及后台运行本地更新.sh'
    echo "用于测试的前三台机器已操作完成，请ssh进入到对应客户机检查结果,输入y继续操作所有机器"
    read -p '继续<发送至所有机器>操作?[y/n]' isContinue
    if [[ "$isContinue" == y ]];then
	 echo "已继续发送至所有机器"
	 echo "（所有机器）第一步：执行客户机 6-完全退出HashMaster.sh 脚本"
	 parallel-ssh -p 8 -h ip_list.txt -P 'bash '$current_path'/HM-LINUX/6-完全退出HashMaster.sh'
	 echo "（所有机器）第二步: 将HM-localip_proxyIp_List.txt移除"
	 parallel-ssh -p 8 -h ip_list_test.txt -P 'bash '$current_path'/E-移除HM-localip_proxyIp_List.txt文件.sh'
	 echo "（所有机器）第三步：执行客户机 1-HashMaster-后台执行程序及后>台运行本地更新.s 脚本"
	 parallel-ssh -p 8 -h ip_list.txt -P 'bash '$current_path'/HM-LINUX/1-HashMaster-后台执行程序及后台运行本地更新.sh'
	 echo "所有机器操作已完成"
	 exit
    fi
    echo "已取消发送至所有机器"
fi
echo "已取消测试操作"
