#!/bin/bash
echo "call D-生成ip_list.txt所有IP的组别--不同组别将使用不同的中转线路.sh"
current_path=$(cd $(dirname $0);pwd)
echo "current_path=$current_path"
cd $current_path

echo "此脚本仅当局域网下某管理员地址超过200台的机器时，不同的机器需要划分为不同的组别, 以使不同的机器使用不同的中转IP列表,避免单个中转IP负载过大而导致无法连接问题"
echo "此脚本将读取ip_list.txt文件，并生成localipKey_groupId_proxyIpList.txt"
echo "HashMaster将在每次重启时，如上一层目录存在localipKey_groupId_proxyIp.txt文件，则将获取其对应的proxyIpList，因此请务必保证目录结构有效"

originIpListFile="./ip_list.txt"
if [ ! -f "$originIpListFile" ];then
    echo "FAILED"
    echo -e "\e[1;33;41m 当前目录不存在 ip_list.txt 文件，请编辑A-一键生成ip_list.txt文件用于批量控制.sh 参数并执行以生成文件.  \e[0m"
    exit
fi

groupCount=3
linesCount=$(wc -l < $originIpListFile)
countsPerGroup=$[($linesCount+$groupCount-1)/$groupCount]
ipList=(ip1,ip2 ip3,ip4 ip5,ip6)
num=${#ipList[@]}
if [ $groupCount -gt $num ]
then 
    echo "ERROR 无足够的可用于分配的IPList"
    exit
fi

echo "机器计划共分为$groupCount组，目前已配置$num组ip, 共有$linesCount台设备, 每组有$countsPerGroup机器"

listIndex=-1
count=0
rm -rf ./HM-localip_proxyIp_List.txt

# 逐行读取文件
while IFS= read -r ip
do
   #count=$((count+1))
   if [ $(expr $count % $countsPerGroup) -eq 0 ]
   then
       listIndex=$((listIndex+1))
       echo -e "\e[1;33;41m 第$listIndex组所配置的IP列表为:${ipList[$listIndex]} 请仔细核对  \e[0m"
   fi
   count=$((count+1))
   # 输出IP地址
   echo "$ip ${ipList[$listIndex]}" >> ./HM-localip_proxyIp_List.txt
done < "$originIpListFile"

