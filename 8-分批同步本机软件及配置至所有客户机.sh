#!/bin/bash
if [ "$(dirname "$(pwd)")" = '/root' ]; then
    echo "上一层目录是 root, 允许执行"
else
    echo "上一层目录不是root，为避免程序运行在非矿机上，必须要求root用户，请修改"
    exit
fi

echo -e "\e[1;33;41m 使用此脚本前，请确保你已正确修改了当前目录下的HM-LINUX的config.ini文件及CoinsConfig下对应币种的ini文件! 通常。需要修改币种/管理员地址/管理员密码/钱包地址等内容.  \e[0m"
sleep 2
echo "call 1-同步本机软件及配置至所有客户机.sh"
current_path=$(cd $(dirname $0);pwd)
echo "current_path=$current_path"
cd $current_path

originIpListFile="./ip_list_A.txt"
if [ ! -f "$originIpListFile" ];then
    echo "FAILED"
    echo -e "\e[1;33;41m 当前目录不存在 ip_list_A.txt 文件，请编辑A-一键生成ip_list_A.txt文件用于批量控制.sh 参数并执行以生成文件.  \e[0m"
    exit
fi

isContinue='y'
echo "将依次执行客户机上6-完全退出HashMaster.sh / 将本机传至客户机 / 1-HashMaster-后台执行程序及后台运行本地更新.sh 操作，请确保IP已经正确配置，输入y继续. 首次操作，将仅操作前三台机器"
read -p '继续<测试>操作?[y/n]' isContinue
if [[ "$isContinue" == y ]];then
    rm -rf HM-LINUX.zip
    zip -r HM-LINUX.zip ./HM-LINUX/ > /dev/null
    echo "（测试）第一步：执行客户机 6-完全退出HashMaster.sh 脚本"
    parallel-ssh -p 8 -h ip_list_test.txt -P 'bash '$current_path'/HM-LINUX/6-完全退出HashMaster.sh'
    echo "（测试）第二步: 将所有本机软件及配置同步至客户机: 复制文件并解压"
    parallel-scp -p 8 -h ip_list_test.txt -l root HM-LINUX.zip $current_path/HM-LINUX.zip
    parallel-ssh -p 8 -h ip_list_test.txt -P 'bash '$current_path'/B-解压HM-LINUX.zip至当前目录.sh' 
    echo "（测试）第三步：执行客户机 1-HashMaster-后台执行程序及后>台运行本地更新.s 脚本"
    parallel-ssh -p 8 -h ip_list_test.txt -P 'bash '$current_path'/HM-LINUX/1-HashMaster-后台执行程序及后台运行本地更新.sh'
    echo "用于测试的前三台机器已操作完成，请ssh进入到对应客户机检查结果,输入y继续操作所有机器"
    read -p '继续<发送至所有机器>操作?[y/n]' isContinue
    if [[ "$isContinue" == y ]];then
	 echo "已继续发送至所有A组机器"
	 echo "（A组机器）第一步：执行客户机 6-完全退出HashMaster.sh 脚本"
	 parallel-ssh -p 32 -h ip_list_A.txt -P 'bash '$current_path'/HM-LINUX/6-完全退出HashMaster.sh'
	 echo "（A组机器）第二步: 将所有本机软件及配置同步至客户机"
	 parallel-scp -p 8 -t 120 -h ip_list_A.txt -l root HM-LINUX.zip $current_path/HM-LINUX.zip
         parallel-ssh -p 32 -h ip_list_A.txt -P 'bash '$current_path'/B-解压HM-LINUX.zip至当前目录.sh'
	 echo "（A组机器）第三步：执行客户机 1-HashMaster-后台执行程序及后>台运行本地更新.s 脚本"
	 parallel-ssh -p 32 -h ip_list_A.txt -P 'bash '$current_path'/HM-LINUX/1-HashMaster-后台执行程序及后台运行本地更新.sh'
	 echo "A组机器操作已完成，等待15秒，将继续自动开始发送至B组机器"
	 
	 sleep 20
         echo "（B组机器）第一步：执行客户机 6-完全退出HashMaster.sh 脚本"
         parallel-ssh -p 32 -h ip_list_B.txt -P 'bash '$current_path'/HM-LINUX/6-完全退出HashMaster.sh'
         echo "（B组机器）第二步: 将所有本机软件及配置同步至客户机"
         parallel-scp -p 8 -t 120 -h ip_list_B.txt -l root HM-LINUX.zip $current_path/HM-LINUX.zip
         parallel-ssh -p 32 -h ip_list_B.txt -P 'bash '$current_path'/B-解压HM-LINUX.zip至当前目录.sh'
         echo "（B组机器）第三步：执行客户机 1-HashMaster-后台执行程序及后>台运行本地更新.s 脚本"
         parallel-ssh -p 32 -h ip_list_B.txt -P 'bash '$current_path'/HM-LINUX/1-HashMaster-后台执行程序及后台运行本地更新.sh'
         echo "B组机器操作已完成，等待15秒，将继续自动开始发送至C组机器"

	 sleep 20
	 echo "（C组机器）第一步：执行客户机 6-完全退出HashMaster.sh 脚本"
         parallel-ssh -p 32 -h ip_list_C.txt -P 'bash '$current_path'/HM-LINUX/6-完全退出HashMaster.sh'
         echo "（C组机器）第二步: 将所有本机软件及配置同步至客户机"
         parallel-scp -p 8 -t 120 -h ip_list_C.txt -l root HM-LINUX.zip $current_path/HM-LINUX.zip
         parallel-ssh -p 32 -h ip_list_C.txt -P 'bash '$current_path'/B-解压HM-LINUX.zip至当前目录.sh'
         echo "（C组机器）第三步：执行客户机 1-HashMaster-后台执行程序及后>台运行本地更新.s 脚本"
         parallel-ssh -p 32 -h ip_list_C.txt -P 'bash '$current_path'/HM-LINUX/1-HashMaster-后台执行程序及后台运行本地更新.sh'
         echo "C组机器操作已完成，等待15秒，将继续自动开始发送至D组机器"

	 sleep 20
	 echo "（D组机器）第一步：执行客户机 6-完全退出HashMaster.sh 脚本"
         parallel-ssh -p 32 -h ip_list_D.txt -P 'bash '$current_path'/HM-LINUX/6-完全退出HashMaster.sh'
         echo "（D组机器）第二步: 将所有本机软件及配置同步至客户机"
         parallel-scp -p 8 -t 120 -h ip_list_D.txt -l root HM-LINUX.zip $current_path/HM-LINUX.zip
         parallel-ssh -p 32 -h ip_list_D.txt -P 'bash '$current_path'/B-解压HM-LINUX.zip至当前目录.sh'
         echo "（D组机器）第三步：执行客户机 1-HashMaster-后台执行程序及后>台运行本地更新.s 脚本"
         parallel-ssh -p 32 -h ip_list_D.txt -P 'bash '$current_path'/HM-LINUX/1-HashMaster-后台执行程序及后台运行本地更新.sh'
         echo "机器操作已完成"

	 rm -rf HM-LINUX.zip
	 exit
    fi
    echo "已取消发送至所有机器"
fi
echo "已取消测试操作"
