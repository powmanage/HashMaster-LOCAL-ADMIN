1- 要求客户机及本机(本地服务器)均为linux系统;
2- 要求客户机及本机的目录结构保持一致:
   
   首次使用时，要求客户机及本机均使用root用户，且目录必须位于~/下执行，依次执行下列命令：
   cd ~
   git clone https://gitlab.com/powmanage/HashMaster-LOCAL-ADMIN.git
   cd HashMaster-LOCAL-ADMIN
   ./0-更新云侧软件至最新版本.sh

3- 需要保证本机可正常免密连接至客户机并无需ssh连接前输入yes确认，具体方案请百度，本机与客户机均应在同一局域网;
4- 需要在本机安装pssh(sudo apt install pssh)
5- 使用无盘时，建议尽量避免大文件更新的操作，避免回写过大;
6- 使用时，建议客户机在首次运行时，提前测试好所有可能需要挖掘的币种（测试时会在Kernel下保存需要用到的内核），以避免后续需要所有客户机在切换币种时下载内核
7- 建议客户机使用默认的hostname作为机器名workername, （即config.ini的workerName不要修改），因此应该正确设置所有客户机的hostname. 这样方便统一管理, 且可以通过同步本机的config.ini 的配置至客户机功能以一键切换币种;
8- 必须支持   locale -a | grep "zh_CN" 可输出zh_CN.utf8， 否则请安装相应语言包。参考注意事项


GPU 安装教程：
1- 使用最新的显卡驱动550.78.(百度网盘)
2- 执行 sudo apt update && sudo apt install gcc make
3- 执行 ./*.run 安装显卡驱动;
4- 使用nvidia-smi查看显卡驱动是否已经安装成功;


Hiveos使用：
1- U-扫描本地所有UBUNTU机器生成ip_list.txt文件用于批量控制.sh
2- 17-初始化远程客户机-包含所有软件及配置.sh 
