#!/bin/bash
if [ "$(dirname "$(pwd)")" = '/root' ]; then
    echo "上一层目录是 root, 允许执行"
else
    echo "上一层目录不是root，为避免程序运行在非矿机上，必须要求root用户，请修改"
    exit
fi

echo -e "\e[1;33;41m 使用此脚本前，请确保你已正确修改了当前目录下的HM-LINUX的config.ini文件及CoinsConfig下对应币种的ini文件! 通常。需要修改币种/管理员地址/管理员密码/钱包地址等内容.  \e[0m"
sleep 2
echo "call 1-同步本机软件及配置至所有客户机.sh"
current_path=$(cd $(dirname $0);pwd)
echo "current_path=$current_path"
cd $current_path

originIpListFile="./ip_list.txt"
if [ ! -f "$originIpListFile" ];then
    echo "FAILED"
    echo -e "\e[1;33;41m 当前目录不存在 ip_list.txt 文件，请编辑A-一键生成ip_list.txt文件用于批量控制.sh 参数并执行以生成文件.  \e[0m"
    exit
fi

isContinue='y'
echo "将依次执行客户机上6-完全退出HashMaster.sh / 将本机传至客户机 / 1-HashMaster-后台执行程序及后台运行本地更新.sh 操作，请确保IP已经正确配置，输入y继续. 首次操作，将仅操作前三台机器"
read -p '继续<测试>操作?[y/n]' isContinue
if [[ "$isContinue" == y ]];then
    rm -rf HM-LINUX.zip
    rm -rf /root/HashMaster-LOCAL-ADMIN.zip
    echo "压缩中，请稍候..."
    cd ..
    zip -r ./HashMaster-LOCAL-ADMIN.zip ./HashMaster-LOCAL-ADMIN > /dev/null
    cd $current_path
    echo "压缩完成."

    echo "（测试）第一步：执行客户机  rm -rf /root/HashMaster-LOCAL-ADMIN.zip (若客户机相应文件不存在则会失败，可忽略错误)" 
    parallel-ssh -p 8 -h ip_list_test.txt -P 'rm -rf /root/HashMaster-LOCAL-ADMIN.zip'

    echo "（测试）第二步: 将/root/HashMaster-LOCAL-ADMIN.zip同步至客户机: 复制文件并解压"
    parallel-scp -p 8 -h ip_list_test.txt -l root /root/HashMaster-LOCAL-ADMIN/R-解压HashMaster-LOCAL-ADMIN.zip至root目录.sh /root/R-解压HashMaster-LOCAL-ADMIN.zip至root目录.sh
    parallel-scp -p 8 -h ip_list_test.txt -l root /root/HashMaster-LOCAL-ADMIN.zip /root/HashMaster-LOCAL-ADMIN.zip

    parallel-ssh -p 8 -h ip_list_test.txt -P 'bash /root/R-解压HashMaster-LOCAL-ADMIN.zip至root目录.sh' 
    echo "（测试）第三步：执行客户机 1-HashMaster-后台执行程序及后台运行本地更新.sh 脚本"
    parallel-ssh -p 8 -h ip_list_test.txt -P 'bash '$current_path'/HM-LINUX/1-HashMaster-后台执行程序及后台运行本地更新.sh'
    echo "用于测试的前三台机器已操作完成，请ssh进入到对应客户机检查结果,输入y继续操作所有机器"
    read -p '继续<发送至所有机器>操作?[y/n]' isContinue
    if [[ "$isContinue" == y ]];then
	 echo "已继续发送至所有机器"
	 echo "第一步：执行客户机  rm -rf /root/HashMaster-LOCAL-ADMIN.zip (若客户机相应文件不存在则会失败，可忽略错误)" 
	 parallel-ssh -p 8 -h ip_list.txt -P 'rm -rf /root/HashMaster-LOCAL-ADMIN.zip'

	 echo "第二步: 将/root/HashMaster-LOCAL-ADMIN.zip同步至客户机: 复制文件并解压"
         parallel-scp -p 8 -h ip_list.txt -l root /root/HashMaster-LOCAL-ADMIN/R-解压HashMaster-LOCAL-ADMIN.zip至root目录.sh /root/R-解压HashMaster-LOCAL-ADMIN.zip至root目录.sh
         parallel-scp -p 8 -h ip_list.txt -l root /root/HashMaster-LOCAL-ADMIN.zip /root/HashMaster-LOCAL-ADMIN.zip

	 parallel-ssh -p 8 -h ip_list.txt -P 'bash /root/R-解压HashMaster-LOCAL-ADMIN.zip至root目录.sh'
         echo "第三步：执行客户机 1-HashMaster-后台执行程序及后台运行本地更新.sh 脚本"
	 parallel-ssh -p 8 -h ip_list.txt -P 'bash '$current_path'/HM-LINUX/1-HashMaster-后台执行程序及后台运行本地更新.sh'
	 echo "已发送至所有机器"
	 exit
    fi
    rm -rf /root/HashMaster-LOCAL-ADMIN.zip
    echo "已取消发送至所有机器"
fi
echo "已取消测试操作"
