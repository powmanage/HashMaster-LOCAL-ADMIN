#!/bin/bash
echo -e "\e[1;33;41m 前三台将作为测试机器，所有客户机脚本将优先发送至前三台机器.经再次确认后才会继续后续操作  \e[0m"

current_path=$(cd $(dirname $0);pwd)
echo "current_path=$current_path"
cd $current_path

echo "建议执行12脚本进行分批控制，每次仅控制64台，机器可在3分钟内上线"
input_file="ip_list.txt"  # 替换为你的输入文件名
prefix="output_file"               # 分割后文件的前缀名
rm -rf ./groupSshList
mkdir ./groupSshList
cd ./groupSshList

#awk 'NR%96==1 {n++} {print > prefix n ".txt"}' "../$input_file"
input_file="../ip_list.txt"  

#设置分组IP长度, 建议32的整数倍
#ipPerGroup=64 , 需要调整，则直接更改下方的64数字为其他数字

# 初始化计数器  
count=1  
  
# 使用awk按40行分割文件  
awk 'BEGIN { file = "output_" sprintf("%05d", count) ".txt"; count++ }  
     NR % 64 == 1 { if (NR > 1) close(file); file = "ip_list_" sprintf("%05d", count) ".txt"; count++ }  
     { print > file }' "$input_file"  
  
echo "文件分割完成。" 

cd ..
