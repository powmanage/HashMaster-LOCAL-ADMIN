nmap -p 22 --open -n 192.168.15.0/24 | grep "Nmap scan report for" | awk '{print $5}' | sort -t. -k1,1n -k2,2n -k3,3n -k4,4n > ip_list.txt

head -3 ip_list.txt > ip_list_test.txt
cat ip_list_test.txt

rm -rf ip_list_A.txt
rm -rf ip_list_B.txt
rm -rf ip_list_C.txt
rm -rf ip_list_D.txt


echo "建议执行12脚本进行分批控制，每次仅控制64台，机器可在3分钟内上线"
input_file="ip_list.txt"  # 替换为你的输入文件名
prefix="output_file"               # 分割后文件的前缀名
rm -rf ./groupSshList
mkdir ./groupSshList
cd ./groupSshList

#awk 'NR%96==1 {n++} {print > prefix n ".txt"}' "../$input_file"
input_file="../ip_list.txt"

#设置分组IP长度, 建议32的整数倍
#ipPerGroup=64 , 需要调整，则直接更改下方的64数字为其他数字

# 初始化计数器  
count=1

# 使用awk按40行分割文件  
awk 'BEGIN { file = "output_" sprintf("%05d", count) ".txt"; count++ }  
     NR % 64 == 1 { if (NR > 1) close(file); file = "ip_list_" sprintf("%05d", count) ".txt"; count++ }  
     { print > file }' "$input_file"

echo "文件分割完成。" 

cd ..

