#!/usr/bin/expect

set timeout 2
set host [lindex $argv 0]
set user [lindex $argv 1]
set password [lindex $argv 2]
set new_root_password [lindex $argv 3]
# 开始 SSH 连接
spawn ssh $user@$host
expect {
    "yes/no" {
        send "yes\r"
        expect "password:"
        send "$password\r"
    }
    "password:" {
        send "$password\r"
    }
}

# 登录成功后执行第一条 sed 命令
expect {
    "# " {
        send "sed -i 's/#   StrictHostKeyChecking ask/StrictHostKeyChecking no/g' /etc/ssh/ssh_config\r"
    }
    "$ " {
        send "sed -i 's/#   StrictHostKeyChecking ask/StrictHostKeyChecking no/g' /etc/ssh/ssh_config\r"
        expect "$ "
    }
}

# 执行第二条 sed 命令
expect {
    "# " {
        send "sed -i 's/#PermitRootLogin prohibit-password/PermitRootLogin yes/g' /etc/ssh/sshd_config\r"
    }
    "$ " {
        send "sed -i 's/#PermitRootLogin prohibit-password/PermitRootLogin yes/g' /etc/ssh/sshd_config\r"
        expect "$ "
    }
}

# 执行 passwd 命令修改 root 密码
expect {
    "# " {
        send "passwd root\r"
        expect {
            "New password:" {
                send "$new_root_password\r"
                expect {
                    "Retype new password:" {
                        send "$new_root_password\r"
                    }
                }
            }
        }
    }
    "$ " {
        send "sudo passwd root\r"
        expect {
            "password for $user:" {
                send "$password\r"
                expect "New password:" {
                    send "$new_root_password\r"
                    expect "Retype new password:" {
                        send "$new_root_password\r"
                    }
                }
            }
        }
    }
}



# 执行 service ssh restart
expect {
    "# " {
        send "service ssh restart\r"
    }
    "$ " {
        send "sudo service ssh restart\r"
        expect "$ "
    }
}

# 确保命令执行完成后退出
expect {
    "# " {
        send "exit\r"
    }
    "$ " {
        send "exit\r"
    }
}

# 确保命令执行完成后退出
expect {
    "# " {
        send "exit\r"
    }
    "$ " {
        send "exit\r"
    }
}

# 关闭交互
expect eof
