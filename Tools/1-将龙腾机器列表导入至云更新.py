import os.path
import os
import sys

'''
本教程用于龙腾导出的机器列表，导入至云更新，需将生成的文件重命名为xx.onc 。
1- 导出的txt，首先复制进入wps表格，然后删除无关项，仅依次保留 名称 ip mac地址三项，保存时，另存为txt，
2- 将txt用记事本打开，使用替换工具，将 tab 替换为空格，然后复制所有内容， 复制到新建的文本文档里，选择ansi保存
3- 用以下python代码处理

'''

with open(os.path.join("File",'dami.txt'), 'w') as wf:
  with open(os.path.join("File",'111.txt'), 'r') as file:
    for line in file:
        elements = line.strip().split()
        mac_addr = elements[-1]
        ip_addr = elements[-2]
        name = elements[0]
        new_format = f"[{mac_addr}]\nIPAddr={ip_addr}\nName={name}\n"
        wf.writelines(new_format)
        #sys.exit()