#!/bin/bash
echo "call 0-HashMaster-前台执行程序.sh"
current_path=$(cd $(dirname $0);pwd)
echo "current_path=$current_path"
cd $current_path
if [ "$(dirname "$(pwd)")" = '/root' ]; then
    echo "上一层目录是 root, 允许执行"
else
    echo "上一层目录不是root，为避免程序运行在非矿机上，必须要求root用户，请修改"
    exit
fi

echo "注意：执行此脚本后，将删除HM-LINUX及HM-LINUX.zip文件，并重新拉取。请仅在确定存在新版时执行此操作, 输入 y 继续，按任意键退出"
isContinue='y'
read -p 'continue?[y/n]' isContinue
if [[ "$isContinue" == y ]];then
    rm -rf ./HM-LINUX.zip
    rm -rf HM-LINUX
    wget https://ubuntu22-kkkkk-1256803747.cos.ap-chongqing.myqcloud.com/HM-LINUX.zip
    unzip HM-LINUX.zip
    rm -rf HM-LINUX.zip
    exit
fi
echo "已取消操作"

