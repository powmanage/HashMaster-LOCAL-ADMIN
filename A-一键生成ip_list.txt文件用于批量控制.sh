#!/bin/bash
echo -e "\e[1;33;41m 前三台将作为测试机器，所有客户机脚本将优先发送至前三台机器.经再次确认后才会继续后续操作  \e[0m"

current_path=$(cd $(dirname $0);pwd)
echo "current_path=$current_path"
cd $current_path

rm -rf ip_list.txt
for num in `echo 172.16.1.{1..250}`;do echo $num >> ip_list.txt;done
for num in `echo 172.16.2.{1..250}`;do echo $num >> ip_list.txt;done
for num in `echo 172.16.3.{1..250}`;do echo $num >> ip_list.txt;done
for num in `echo 172.16.4.{1..50}`;do echo $num >> ip_list.txt;done

head -3 ip_list.txt > ip_list_test.txt

rm -rf ip_list_A.txt
rm -rf ip_list_B.txt
rm -rf ip_list_C.txt
rm -rf ip_list_D.txt

for num in `echo 172.16.1.{1..250}`;do echo $num >> ip_list_A.txt;done
for num in `echo 172.16.2.{1..250}`;do echo $num >> ip_list_B.txt;done
for num in `echo 172.16.3.{1..250}`;do echo $num >> ip_list_C.txt;done
for num in `echo 172.16.4.{1..50}`;do echo $num >> ip_list_D.txt;done

head -1 ip_list_B.txt >> ip_list_test.txt
head -1 ip_list_C.txt >> ip_list_test.txt
head -1 ip_list_D.txt >> ip_list_test.txt

echo "建议执行12脚本进行分批控制，每次仅控制64台，机器可在3分钟内上线"
input_file="ip_list.txt"  # 替换为你的输入文件名
prefix="output_file"               # 分割后文件的前缀名
rm -rf ./groupSshList
mkdir ./groupSshList
cd ./groupSshList

#awk 'NR%96==1 {n++} {print > prefix n ".txt"}' "../$input_file"
input_file="../ip_list.txt"  

#设置分组IP长度, 建议32的整数倍
#ipPerGroup=64 , 需要调整，则直接更改下方的64数字为其他数字

# 初始化计数器  
count=1  
  
# 使用awk按40行分割文件  
awk 'BEGIN { file = "output_" sprintf("%05d", count) ".txt"; count++ }  
     NR % 64 == 1 { if (NR > 1) close(file); file = "ip_list_" sprintf("%05d", count) ".txt"; count++ }  
     { print > file }' "$input_file"  
  
echo "文件分割完成。" 

cd ..
