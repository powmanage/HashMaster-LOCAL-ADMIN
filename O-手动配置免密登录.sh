#!/bin/bash  
  
# 假设txt文件名为ips.txt，且在同一目录下  
FILE="./ip_list.txt"
PASSWD="12345"
echo "应首先客户机侧执行./F 及 passwd root修改密码，然后再执行此脚本"
#destDir="./202406041401/"
#echo destDir:$destDir
#mkdir $destDir
#echo "sleep 20s"
#sleep 20
#sshpass -p 'root123!@#' parallel-ssh -p 8 -h ips.txt -P 'cd /root/ceremonyclient/node/ && ./node-1.4.18-linux-amd64 -peer-id > .config/wallet.txt'
#echo "sleep 20s"
#sleep 20
# 假设源文件和目标路径、用户名以及密码（注意：密码直接写在脚本中是不安全的，这里仅为示例）  
#SOURCE_FILE="/root/ceremonyclient/node/.config"

# 读取文件中的每一行（假设每行都是一个IP地址）  
while IFS= read -r ip  
do
    time sleep 1
    # 构造目标主机的完整SSH地址（如果需要的话）  
    # 在这个例子中，我们假设DEST_HOST是固定的，但你可以根据ip来设置它  
    # 例如：DEST_HOST="$ip"  
    #rm -rf $destDir$ip
    #mkdir $destDir$ip
    # 使用sshpass和scp进行文件传输（注意：sshpass可能不在所有系统上默认安装）  
    # 为了安全起见，建议使用SSH密钥而不是密码  
    sshpass -p $PASSWD scp -o ConnectTimeout=30 -r /root/.ssh/id_rsa.pub root@$ip:/root/.ssh/authorized_keys
    # 检查上一个命令的退出状态，并据此采取行动  
    if [ $? -ne 0 ]; then
        echo "Error copying to $ip"  
    else
        echo "Copied to $ip successfully"  
    fi
done < "$FILE"

