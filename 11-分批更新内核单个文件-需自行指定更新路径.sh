echo "请务必确保 J-停止Coin-Running后台.sh  及 K-运行Coin-Running后台.sh 设置正确"

if [ "$(dirname "$(pwd)")" = '/root' ]; then
    echo "上一层目录是 root, 允许执行"
else
    echo "上一层目录不是root，为避免程序运行在非矿机上，必须要求root用户，请修改"
    exit
fi

current_path=$(cd $(dirname $0);pwd)
echo "current_path=$current_path"
cd $current_path

updateFilepath=/root/HashMaster-LOCAL-ADMIN/HM-LINUX/Kernel/rqiner/newest/rqiner-x86
chmod +x $updateFilepath
echo -e "\e[1;33;41m 将本机下列文件传输到客户机并重启所有客户机软件，请务必确保文件正确:$updateFilepath \e[0m"

originIpListFile="./ip_list.txt"
if [ ! -f "$originIpListFile" ];then
    echo "FAILED"
    echo -e "\e[1;33;41m 当前目录不存在 ip_list.txt 文件，请编辑A-一键生成ip_list.txt文件用于批量控制.sh 参数并执行以生成文>件.  \e[0m"
    exit
fi

isContinue='y'
echo -e "\e[1;33;41m 第一步：将J-停止Coin-Running后台.sh 及 K-运行Coin-Running后台.sh 发送至所有客户机（可选）  \e[0m"
echo "输入y继续. 将操作所有机器，如果客户机J-停止Coin-Running后台.sh 及 K-运行Coin-Running后台.sh 无需更新，请输入n"
read -p '继续发送<测试>操作?[y/n]' isContinue
if [[ "$isContinue" == y ]];then
   parallel-scp -p 8 -h ip_list.txt -l root J-停止Coin-Running后台.sh /root/HashMaster-LOCAL-ADMIN/J-停止Coin-Running后台.sh
   parallel-scp -p 8 -h ip_list.txt -l root K-运行Coin-Running后台.sh /root/HashMaster-LOCAL-ADMIN/K-运行Coin-Running后台.sh
fi

echo -e "\e[1;33;41m 第二步（测试）：依次执行J-停止Coin-Running后台.sh。传输新文件至客户机 及 K-运行Coin-Running后台.sh \e[0m"
echo "输入y继续. 将操作前三台机器"
read -p '继续发送<测试>操作?[y/n]' isContinue
if [[ "$isContinue" == y ]];then
   parallel-ssh -h ip_list_test.txt -P 'bash /root/HashMaster-LOCAL-ADMIN/J-停止Coin-Running后台.sh'
   parallel-scp -p 8 -h ip_list_test.txt -l root $updateFilepath $updateFilepath
   parallel-ssh -h ip_list_test.txt -P 'bash /root/HashMaster-LOCAL-ADMIN/K-运行Coin-Running后台.sh'
   echo -e "\e[1;33;41m 执行测试已完成，请检查  \e[0m"
   echo -e "\e[1;33;41m 第三步（所有,A/B/C/D分组执行）：  \e[0m"
   echo “输入y继续. 将操作所有机器”
   read -p '继续<所有机器>操作?[y/n]' isContinue
   if [[ "$isContinue" == y ]];then
     if [ -f "ip_list_A.txt" ];then
       echo "A组"
       parallel-ssh -h ip_list_A.txt -P 'bash /root/HashMaster-LOCAL-ADMIN/J-停止Coin-Running后台.sh'
       parallel-scp -p 8 -h ip_list_A.txt -l root $updateFilepath $updateFilepath
       parallel-ssh -h ip_list_A.txt -P 'bash /root/HashMaster-LOCAL-ADMIN/K-运行Coin-Running后台.sh'
     fi

     if [ -f "ip_list_B.txt" ];then
       echo "B组"
       parallel-ssh -h ip_list_B.txt -P 'bash /root/HashMaster-LOCAL-ADMIN/J-停止Coin-Running后台.sh'
       parallel-scp -p 8 -h ip_list_B.txt -l root $updateFilepath $updateFilepath
       parallel-ssh -h ip_list_B.txt -P 'bash /root/HashMaster-LOCAL-ADMIN/K-运行Coin-Running后台.sh'
     fi

     if [ -f "ip_list_C.txt" ];then
       echo "C组"
       parallel-ssh -h ip_list_C.txt -P 'bash /root/HashMaster-LOCAL-ADMIN/J-停止Coin-Running后台.sh'
       parallel-scp -p 8 -h ip_list_C.txt -l root $updateFilepath $updateFilepath
       parallel-ssh -h ip_list_C.txt -P 'bash /root/HashMaster-LOCAL-ADMIN/K-运行Coin-Running后台.sh'
     fi

      if [ -f "ip_list_D.txt" ];then
       echo "D组"
       parallel-ssh -h ip_list_D.txt -P 'bash /root/HashMaster-LOCAL-ADMIN/J-停止Coin-Running后台.sh'
       parallel-scp -p 8 -h ip_list_D.txt -l root $updateFilepath $updateFilepath
       parallel-ssh -h ip_list_D.txt -P 'bash /root/HashMaster-LOCAL-ADMIN/K-运行Coin-Running后台.sh'
     fi
   fi
fi

