#!/bin/bash
current_path=$(cd $(dirname $0);pwd)
echo "current_path=$current_path"
cd $current_path
PASSWORD="123456"
USER="root"

echo "YOUR USER IS $USER, PASSWORD IS $PASSWORD"
sleep 5.

FILE="./ip_list.txt"
# 读取文件中的每一行（假设每行都是一个IP地址）
while IFS= read -r ip
do
    ./S-expect-to-hiveos-init.sh $ip user 1 $PASSWORD
    time sleep 1
    # 为了安全起见，建议使用SSH密钥而不是密码
    sshpass -p $PASSWORD ssh-copy-id $USER@$ip
    # 检查上一个命令的退出状态，并据此采取行动
done < "$FILE"
