#!/bin/bash
if [ "$(dirname "$(pwd)")" = '/root' ]; then
    echo "上一层目录是 root, 允许执行"
else
    echo "上一层目录不是root，为避免程序运行在非矿机上，必须要求root用户，请修改"
    exit
fi

echo "call 5-更新所有客户机的HashMaster-LOCAL-ADMIN仓.sh"
current_path=$(cd $(dirname $0);pwd)
echo "current_path=$current_path"
cd $current_path

isContinue='y'
echo "将执行客户机上 C-更新当前程序至最新版.sh，输入y继续. 首次操作，将仅操作前三台机器"
read -p '继续<测试>操作?[y/n]' isContinue
if [[ "$isContinue" == y ]];then
    parallel-ssh -p 8 -h ip_list_test.txt -P 'bash '$current_path'/C-更新当前程序至最新版.sh'
    echo "用于测试的前三台机器已操作完成，请ssh进入到对应客户机检查结果,输入y继续操作所有机器"
    read -p '继续<发送至所有机器>操作?[y/n]' isContinue
    if [[ "$isContinue" == y ]];then
	 echo "已继续发送至所有机器"
	 parallel-ssh -p 8 -h ip_list.txt -P 'bash '$current_path'/C-更新当前程序至最新版.sh'
	 echo "所有机器操作已完成"
	 exit
    fi
    echo "已取消发送至所有机器"
fi
echo "已取消测试操作"
