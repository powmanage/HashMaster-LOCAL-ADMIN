echo "请务必确保H-降低线程.sh 及 I-恢复线程至正常值.sh设置正确"
if [ "$(dirname "$(pwd)")" = '/root' ]; then
    echo "上一层目录是 root, 允许执行"
else
    echo "上一层目录不是root，为避免程序运行在非矿机上，必须要求root用户，请修改"
    exit
fi

sleep 2
current_path=$(cd $(dirname $0);pwd)
echo "current_path=$current_path"
cd $current_path

originIpListFile="./ip_list.txt"
if [ ! -f "$originIpListFile" ];then
    echo "FAILED"
    echo -e "\e[1;33;41m 当前目录不存在 ip_list.txt 文件，请编辑A-一键生成ip_list.txt文件用于批量控制.sh 参数并执行以生成文>件.  \e[0m"
    exit
fi

isContinue='y'
echo -e "\e[1;33;41m 第一步：将I-恢复线程至正常值.sh 及 H-降低线程.sh 发送至测试客户机并执行测试（可选）  \e[0m"
echo "输入y继续. 首次操作，将仅操作前三台机器"
read -p '继续发送<测试>操作?[y/n]' isContinue
if [[ "$isContinue" == y ]];then
   parallel-scp -p 8 -h ip_list_test.txt -l root I-恢复线程至正常值.sh /root/HashMaster-LOCAL-ADMIN/I-恢复线程至正常值.sh
   parallel-scp -p 8 -h ip_list_test.txt -l root H-降低线程.sh /root/HashMaster-LOCAL-ADMIN/H-降低线程.sh
   parallel-ssh -h ip_list_test.txt -P 'bash /root/HashMaster-LOCAL-ADMIN/H-降低线程.sh'
   echo -e "\e[1;33;41m 执行测试中，已执行测试机上 H-降低线程.sh' 请在20秒内检查是否线程已降低  \e[0m"
   sleep 20
   parallel-ssh -h ip_list_test.txt -P 'bash /root/HashMaster-LOCAL-ADMIN/I-恢复线程至正常值.sh'
   echo -e "\e[1;33;41m 执行测试中，已执行测试机上 I-恢复线程至正常值.sh' 请在20秒内检查是否线程已降低  \e[0m"
   sleep 20 
   echo -e "\e[1;33;41m 第二步：将I-恢复线程至正常值.sh 及 H-降低线程.sh 发送至客户机（可选）  \e[0m"
   echo “输入y继续. 将操作所有机器，下发H-降低线程.sh 及 I-恢复线程至正常值.sh 至客户机”
   read -p '继续<所有机器>操作?[y/n]' isContinue
   if [[ "$isContinue" == y ]];then
     parallel-scp -p 8 -h ip_list.txt -l root I-恢复线程至正常值.sh /root/HashMaster-LOCAL-ADMIN/I-恢复线程至正常值.sh
     parallel-scp -p 8 -h ip_list.txt -l root H-降低线程.sh /root/HashMaster-LOCAL-ADMIN/H-降低线程.sh
   fi
fi

echo "发送已完成（或已取消），将循环执行脚本\n\n\n"
echo -e "\e[1;33;41m 第三步：循环定时执行所有客户机的 I-恢复线程至正常值.sh 及 H-降低线程.sh  \e[0m"
echo "输入y继续. 将操作所有机器"
read -p '继续<所有机器循环执行>操作?[y/n]' isContinue
if [[ "$isContinue" == y ]];then
    echo “脚本将循环执行，将在10:00执行所有客户机H-降低线程.sh，20:00执行I-恢复线程至正常值.sh”
    now=$(date +%H:%M)

    while true; do
      now=$(date +%H:%M)
      if [ "$now" == "10:00" ]; then
	echo "现在时间为 10:00， 执行H-降低线程.sh"
	sleep 10
        parallel-ssh -h ip_list.txt -P 'bash /root/HashMaster-LOCAL-ADMIN/H-降低线程.sh' 
        sleep 80
	echo "执行H-降低线程.sh已完成，将在20：00执行I-恢复线程至正常值.sh"
      fi
      
      if [ "$now" == "20:00" ]; then
        echo "现在时间为 20:00, 执行I-恢复线程至正常值.sh"
        sleep 10
        parallel-ssh -h ip_list.txt -P 'bash /root/HashMaster-LOCAL-ADMIN/I-恢复线程至正常值.sh'
	sleep 80
	echo "执行 I-恢复线程至正常值.sh 已完成，将在10:00执行H-降低线程.sh"
      fi
      echo "现在时间为：" $now
      sleep 10
    done
fi
echo “取消循环执行”
